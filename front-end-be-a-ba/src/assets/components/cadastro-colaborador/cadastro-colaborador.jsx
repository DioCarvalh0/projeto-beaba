import '../cadastro-colaborador/cadastro-colaborador.css'

function CadastroColaborador(){
    return(
        <>
            <body>
                <div className='backgroundPerCent'></div>
                <div className='allPage'>
                    <h1 className='fontConfig'>CADASTRO</h1>
                    <div className='componentsCadastro'>
                        <input className='inputCadastroName' type="text" id='firstName' placeholder='Primeiro Nome'/>
                        <input className='inputCadastroName' type="text" id='lastName' placeholder='Sobrenome'/>
                        <input className='inputCadastro' type="email" id='email' placeholder='Email'/>
                        <input className='inputCadastro' type="text" id='matricula' placeholder='Matrícula'/>
                        <select className='selectComponent' name='tipoContrato' id=''>
                            <option value="" disabled selected hidden>Tipo de Contrato</option>
                            <option value="CLT">CLT</option>
                            <option value="PJ">PJ</option>
                        </select>
                        <input className='inputCadastro' type="text" id='cpfCnpj' placeholder='CPF/CNPJ'/>
                        <select className='selectComponent' name='cargo' id=''>
                            <option value="" disabled selected hidden>Cargo</option>
                            <option value="DevJR">Desenvolvedor de Software Jr.</option>
                            <option value="Gestor">Gestor</option>
                        </select>
                        <select className='selectComponent' name='gestor' id=''>
                            <option value="" disabled selected hidden>Gestor</option>
                            <option value="gestorDiogo">Diogo</option>
                            <option value="gestorGustavo">Gustavo</option>
                        </select>
                        <button className='buttonCadastrar'>CADASTRAR</button>
                    </div>
                </div>
            </body>
        </>
    )
}

export default CadastroColaborador